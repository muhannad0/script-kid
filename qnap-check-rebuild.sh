#!/bin/bash
# Mohamed Muhannad
# Takes the /proc/mdstat recovery info and emails progress. You can add the script to a cron job to run it at regular intevals
# Usage: ./qnap-check-rebuild.sh
 
recipient=sender@example.com
sender=email@example.com
subject="NAS - Rebuild Status"
tmpfile=/tmp/rebuild-email-msg.tmp


#status=$(cat /proc/mdstat | awk '/raid6/{x=NR+2}(NR<=x){print}')
#get status
status=$(egrep recovery /proc/mdstat)
#get timeleft, round up to integer
timeLeftMins=$(egrep recovery /proc/mdstat | awk '{print $6}' | sed 's/[^0-9.]//g' | awk '{printf("%d\n",$1 + 0.5)}')
#get progress
progress=$(egrep recovery /proc/mdstat | awk '{print $4}')

#echo -e "$(($timeLeftMins / 60)) hours"
#echo -e "$progress complete"

echo To: $recipient > $tmpfile
echo From: $sender >> $tmpfile
echo Subject: $subject - $progress >> $tmpfile
echo "" >> $tmpfile
echo "Approximately $(($timeLeftMins / 60)) hours to complete" >> $tmpfile
echo "" >> $tmpfile
echo "$status" >> $tmpfile
echo "" >> $tmpfile
echo "[`date +%d/%b/%Y:%H:%M:%S`]" >> $tmpfile
echo "" >> $tmpfile

cat $tmpfile | /usr/sbin/ssmtp -v $recipient

rm $tmpfile
