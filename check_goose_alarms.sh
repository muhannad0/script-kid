#!/bin/bash
# Mohamed Muhannad
# For Linux
# Dependencies: xmllint package
# Script to monitor certain alarms and variables on WeatherGoose II environment monitoring device.
# When alarm is triggered can ouput to any messaging service; like email, sms api, etc.

#wlog file location
#LOG="~/goose-alerts.log"

#Check for missing parameters

if [[ -z "$1" ]] || [[ -z "$2" ]]; then
	echo "Missing parameters! Syntax: ./check_goose_alerts.sh IP ALARMID"
	exit 2
fi

#WEATHERGOOSE XML URL DEFINITION
GOOSE_URL="http://$1/data.xml"

#XML ATTRIBUTE DEFINITIONS
#sensor definition
sensor="//server/devices/device/field"
#alarm definition
alarmArray="//server/alarms/alarm[@alarm-num="$2"]"

#input xml defn from alarmArray, return alarm name and status
function checkAlarm()
{
	local alarmName=($(xmllint --xpath $1/@field $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}'))
	local alarmStatus=($(xmllint --xpath $1/@status $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}'))
	local alarmDevice=($(xmllint --xpath $1/@device-id $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}'))
	#local alarmRepeat=($(xmllint --xpath $1/@status $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}'))
	echo $alarmName:$alarmStatus:$alarmDevice
}

#input xml defn from alarmArray and alarmName from checkAlarm function, return details message
function getAlarmDetails()
{
	local alarmCurrentStatus=$(xmllint --xpath $1/@status $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}')
	#local alarmLimitType=$(xmllint --xpath $1/@limtype $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}')
	local alarmThreshold=$(xmllint --xpath $1/@limit $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}')
	local alarmNiceName=$(xmllint --xpath //server/devices/device[@id="'$3'"]/field[@key="'$2'"]/@niceName $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}')
	local alarmCurrentValue=$(xmllint --xpath //server/devices/device[@id="'$3'"]/field[@key="'$2'"]/@value $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}')
	#local alarmCurrentTime=$(date +%F' '%T)
	#local alarmMessage="$alarmCurrentTime\n$alarmNiceName:$alarmCurrentStatus\nCurrent Value:$alarmCurrentValue\nThreshold:$alarmThreshold\nType:$alarmLimitType\n"
	local alarmMessage="$alarmNiceName - Status:$alarmCurrentStatus Current Value:$alarmCurrentValue Threshold:$alarmThreshold"
	echo $alarmMessage
}

result=$(checkAlarm $alarmArray)
#echo $result
name=$(echo $result | awk -F'[:]' '{print $1}')
status=$(echo $result | awk -F'[:]' '{print $2}')
device=$(echo $result | awk -F'[:]' '{print $3}')
#echo $name $status $device

#if untripped
if [ $status = "Untripped" ]
	then
		details=$(getAlarmDetails $alarmArray $name $device)
		echo "$details"
		exit 0
fi

#if tripped
if [ $status = "Tripped" ]
        then
                details=$(getAlarmDetails $alarmArray $name $device)
                echo "$details"
                exit 2
fi
