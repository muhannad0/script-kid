#!/bin/bash
# Mohamed Muhannad
# For Linux
# Dependencies: a MySQL database installation
# Script to backup MySQL databases on a server and send notification.
# Specific requirement included to copy backup files from local disk to NAS folder attached on server.

# Setup initial variables
# Database credentials
user="root"
password="root"
host="127.0.0.1"

# Setup email recipient
recipient=user@example.com
subject="MySQL Backup Job"

# Set root backup path
backupPath="$HOME/db_backups/"
# Set current date and time
date=$(date +%Y-%m-%d)
time=$(date +%H-%M-%S)

# Setup backup directory
backupDir="$backupPath$date/"

# Setup backup file name
backupFile=$host"_"$date"_"$time

# Setup log
logFile="/tmp/db_backup_log_$date"

# Setup log file contents
touch $logFile
echo -e "Database Backup Notification Log" >> $logFile
echo -e "==================================" >> $logFile
echo -e "Server: $HOSTNAME / $host" >> $logFile
echo -e "Date/Time: $date $time\n" >> $logFile

# Time Converter function
convertSeconds (){
	h=$(($1/3600))
	m=$((($1/60)%60))
	s=$(($1%60))
	printf "%02d:%02d:%02d\n" $h $m $s
}

# Normal DB backup function
backupDatabase () {
        #mysqldump -u $1 --password="$2" --quick --extended-insert --lock-tables --allow-keywords --databases $3 > $4.$3.sql
	touch $4.$3.sql
}

# File compress function
compressFile () {
	tar -jcvf $1.tar.bz2 $1
}

# Get script start time
scriptStart=$(date +%s)

# Setup list of databases to backup (normal backup)
dbList=(
db1
db2
db3
db4
)

# Check if dbList is empty
if [ -z $dbList ]; then
	echo -e "DB List Empty"
	exit
else

# Do backup
# Create backup working directory
mkdir -p ${backupDir}
cd ${backupDir}

printf "%-20s %s\n" "Database" "Duration" >> $logFile

# Start backup
for database in ${dbList[@]}; do
	functionStart=$(date +%s)

	backupDatabase $user $password $database $backupFile
#	sleep 1

	functionEnd=$(date +%s)
	functionElapsedTime=$(($functionEnd-$functionStart))
	functionDuration=$(convertSeconds $functionElapsedTime)
	printf "%-20s %s\n" "$database" "$functionDuration" >> $logFile 

done

# Do compression and sql file cleanup
# Get all files in backup folder
shopt -s nullglob
fileArray=( * )

for file in ${fileArray[@]}; do
	#echo -e "$file"
	compressFile $file
	rm $file
done


printf "\n%-100s %s\n" "Filename" "Size" >> $logFile

# Do filesizes
# Get all files in backup folder
shopt -s nullglob
fileArray=( * )

for file in ${fileArray[@]}; do
	filesize=$(ls -lh $file | awk ' { print $5 }')
	printf "%-100s %s\n" "$file" "$filesize" >> $logFile
done

# TODO Copy backup files to offsite storage

fi

# Get script end time
scriptEnd=$(date +%s)
# Calculate elapsed time
scriptElapsedTime=$(($scriptEnd-$scriptStart))

echo -e "\nDatabase backup completed in $(($scriptElapsedTime / 60)) minutes and $(($scriptElapsedTime % 60)) seconds." >> $logFile

# Send email alert

cat $logFile | mail -s "$subject" $recipient

rm $logFile
