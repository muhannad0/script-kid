#!/bin/bash
# Mohamed Muhannad
# Small script to keep the system without going to standby.
# Usage: ./osx-keep-awake.sh <minutes> Run script with number of minutes you want to keep system awake.

minutes=$1
((seconds=minutes * 60))
echo Staying awake for $minutes minutes
caffeinate -u -t $seconds
