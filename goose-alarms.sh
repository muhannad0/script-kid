#!/bin/bash
# Mohamed Muhannad
# For Ubuntu 14.04 LTS
# Dependencies: xmllint package
# Script to monitor certain alarms and variables on WeatherGoose II environment monitoring device.
# When alarm is triggered can ouput to any messaging service; like email, sms api, etc.
# Usage: ./goose-alarms.sh <IP address of WeatherGooseII device. Script will keep running in foreground.

#wlog file location
LOG="~/goose-alerts.log"

#WEATHERGOOSE XML URL DEFINITION
GOOSE_URL="http://$1/data.xml"

#XML ATTRIBUTE DEFINITIONS
#sensor definition
sensor="//server/devices/device/field"
#alarm definition
alarmArray[0]="//server/alarms/alarm[@alarm-num="0"]"
alarmArray[1]="//server/alarms/alarm[@alarm-num="1"]"

#Alarm Global Variables declare
alarmNotify[0]=false
alarmNotify[1]=false

alarmNotifyInterval[0]=3
alarmNotifyInterval[1]=3

alarmTime[0]=0
alarmTime[1]=0

#Check alarms every 10 seconds
checkInterval=10

#run script
runScript=true

#input xml defn from alarmArray, return alarm name and status
function checkAlarm()
{
	local alarmName=($(xmllint --xpath $1/@field $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}'))
	local alarmStatus=($(xmllint --xpath $1/@status $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}'))
	#local alarmRepeat=($(xmllint --xpath $1/@status $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}'))
	echo $alarmName:$alarmStatus
}

#input xml defn from alarmArray and alarmName from checkAlarm function, return details message
function getAlarmDetails()
{
	local alarmCurrentStatus=$(xmllint --xpath $1/@status $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}')
	local alarmLimitType=$(xmllint --xpath $1/@limtype $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}')
	local alarmThreshold=$(xmllint --xpath $1/@limit $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}')
	local alarmNiceName=$(xmllint --xpath $sensor[@key="'$2'"]/@niceName $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}')
	local alarmCurrentValue=$(xmllint --xpath $sensor[@key="'$2'"]/@value $GOOSE_URL | awk -F'[="]' '{print $(NF-1)}')
	local alarmCurrentTime=$(date +%F' '%T)
	local alarmMessage="$alarmCurrentTime\n$alarmNiceName:$alarmCurrentStatus\nCurrent Value:$alarmCurrentValue\nThreshold:$alarmThreshold\nType:$alarmLimitType\n"
	echo $alarmMessage
}

while [ $runScript = true ]
do
	for alarm in ${!alarmArray[@]}
	do
		result=$(checkAlarm ${alarmArray[$alarm]})
		name=$(echo $result | awk -F'[:]' '{print $1}')
		status=$(echo $result | awk -F'[:]' '{print $2}')
		#echo $alarm $name $status

		#if untripped
		if [ $status = "Untripped" ]
		then
			#but check for a status change
			if [ ${alarmTime[$alarm]} -gt 0 ]
			then
				details=$(getAlarmDetails ${alarmArray[$alarm]} $name)
				#echo -e "$details\n"
				alarmNotify[$alarm]=true
				alarmTime[$alarm]=0
			else
				alarmNotify[$alarm]=false
				alarmTime[$alarm]=0
				#echo -e ${alarmNotify[$alarm]}
			fi
		#if tripped
		elif [ $status = "Tripped" ]
		then
			#start alarm notification elapsed time
			((alarmTime[$alarm]++))
			#echo -e ${alarmTime[$alarm]}
			#check if it is first alert and until the next alert
			if [ ${alarmTime[$alarm]} -eq 1 ] || [ $((${alarmTime[$alarm]} % ${alarmNotifyInterval[$alarm]})) -eq 0 ]
			then
				alarmNotify[$alarm]=true
				details=$(getAlarmDetails ${alarmArray[$alarm]} $name)
			#stop notifications even though alert status
			else
				alarmNotify[$alarm]=false
			fi
		fi

		#send alert notification
		if [ ${alarmNotify[$alarm]} = true ]
		then
			echo -e "WeatherGoose-AlliedServerRoom\n-----\n$details"
		fi
	done

	sleep $checkInterval

done
