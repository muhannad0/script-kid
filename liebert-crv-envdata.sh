#!/bin/bash
# Mohamed Muhannad
# Dependencies: snmp tools package
# Script to monitor metrics of Liebert CRV Cooling Units
# Stores monitoring data to a file
# Usage: run as a cron job at regular intervals (eg: 5 mins)
# ./liebert-crv-envdata.sh <snmp_community> <host ip>

# Required inputs: SNMP community and host to check
snmpCommunity=$1
crvUnit=$2

# Log file location
logDir="/tmp"
logFile=""

# Metrics OID Definitions
declare -A crvOidArray
crvOidArray[fanspeed]=".1.3.6.1.4.1.476.1.42.3.4.3.16.0"
crvOidArray[cooling]=".1.3.6.1.4.1.476.1.42.3.4.3.9.0"
crvOidArray[supplyairtemp]=".1.3.6.1.4.1.476.1.42.3.9.20.1.20.1.2.1.5002"
crvOidArray[returnairtemp]=".1.3.6.1.4.1.476.1.42.3.9.20.1.20.1.2.1.4291"
crvOidArray[airtempsetpoint]=".1.3.6.1.4.1.476.1.42.3.9.20.1.20.1.2.1.5008"
crvOidArray[supplyhumid]=".1.3.6.1.4.1.476.1.42.3.9.20.1.20.1.2.1.5027"
crvOidArray[returnhumid]=".1.3.6.1.4.1.476.1.42.3.9.20.1.20.1.2.1.5028"
crvOidArray[humidsetpoint]=".1.3.6.1.4.1.476.1.42.3.9.20.1.20.1.2.1.5029"
crvOidArray[highreturnhumidthreshold]=".1.3.6.1.4.1.476.1.42.3.9.20.1.20.1.2.1.5033"
crvOidArray[lowreturnhumidthreshold]=".1.3.6.1.4.1.476.1.42.3.9.20.1.20.1.2.1.5035"

# Get SNMP Values Function
# Inputs: snmp community, host ip, oid

function getSnmpData () {
	local community="$1"
	local hostip="$2"
	local oid="$3"
	# Get value of oid
	local metricString=$(snmpget -Oqv -v2c -c $community $hostip $oid)
	# clear any double quotes in return value
	local metricValue=$(sed -e 's/^"//' -e 's/"$//' <<<"$metricString")
	# set any string return value (unavailable) to zero
	if [[ $metricValue = "Unavailable" ]]
	then
		metricValue=0
	fi
	# return the value
	echo $metricValue

}

function createLogFile () {
        logFileName=$1_$(date +%Y-%m-%d)
        logFile=$logDir/$logFileName
	# Check if log file present, if not create it
        if [ ! -f $logFile ]
        then
                echo "Log file not found! Creating log file..."
                #echo $logFile
                touch $logFile
        fi
}

# Get SNMP OID and Data for CRV and Log Values
# Inputs: snmp community, host ip

function getSnmp() {
	# Set host identifier from SNMP data
	local hostIdent=$(getSnmpData $1 $2 "sysName.0")

	createLogFile $hostIdent
	
	# Get values for oid definitions in array
	for oid in "${!crvOidArray[@]}"
	do
		local timeStamp=$(date "+%Y-%m-%d %T")
		local data=$(getSnmpData $1 $2 ${crvOidArray[$oid]})
		# data record save to log file
		echo $timeStamp,$hostIdent,$oid,$data >> $logFile
		#echo $timeStamp,$hostIdent,$oid,$data
	done
}

# Run the script
getSnmp $snmpCommunity $crvUnit
